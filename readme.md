## 仓库简介

数字化转型，云是基石，安全当先。华为云视“安全、稳定、高质量”为生命线，践行DevSecOps，构建安全可信的云服务。同时华为云把全球开服积累的安全合规经验和技术能力服务化，打造云原生安全服务并聚合生态产品方案和能力，帮助租户构建立体的安全体系和智能高效的安全运营体系。

#  项目总览

<table style="text-align: center">
    <tr style="font-weight: bold">
        <td>项目</td>
        <td>介绍</td>
        <td>仓库</td>
    </tr>
     <tr>
        <td rowspan="3">云证书管理</td>
        <td>申请终端实体证书，并通过API查询正式详情与吊销证书</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-ccm-pca-cert-manager-java">huaweicloud-CCM-PCA-CertManager-java</a></td>
    </tr> 
    <tr>
        <td>申请CA证书，并通过API查询CA详情与导出CA证书体</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-ccm-pca-camanager-java">huaweicloud-CCM-PCA-CAManager-java</a></td>
      </tr> 
    <tr>
        <td>授予CCM服务OBS访问权限，以便开启CRL功能</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-ccm-pca-crl-manager-java">huaweicloud-CCM-PCA-CrlManager-java</a></td>
      </tr>  
    <tr>
        <td rowspan="3">云防火墙CFW</td>
        <td>演示如何使用过防火墙ips功能</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cfw-ips-java">huaweicloud-cfw-ips-java</a></td>
    </tr> 
    <tr>
        <td>演示了如何调用cfw-java-sdk来创建创建黑白名单并提交审核</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cfw-blackwhitelist-java">huaweicloud-cfw-blackwhitelist-java</a></td>
      </tr> 
    <tr>
        <td>演示了如何如何使用防火墙acl rule功能</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cfw-acl-rule-java">huaweicloud-cfw-acl-rule-java</a></td>
      </tr>
    <tr>
        <td rowspan="2">数据库安全服务DBSS</td>
        <td>展示如何通过java版SDK查询租户配额信息</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-show-audit-quota-java">huaweicloud-show-audit-quota-java</a></td>
    </tr> 
    <tr>
        <td>展示如何通过java版SDK查询审计实例列表</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-list-audit-instances-java">huaweicloud-list-audit-instances-java</a></td>
      </tr> 
            </td>
</tr>
</table>







​        







